/*
 * Copyright 2015 Matthieu Gallien <matthieu_gallien@yahoo.fr>
 *
 * This library is free software; you can redistribute it and/or
 * modify it under the terms of the GNU Library General Public
 * License as published by the Free Software Foundation; either
 * version 3 of the License, or (at your option) any later version.
 *
 * This library is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU
 * Library General Public License for more details.
 *
 * You should have received a copy of the GNU Library General Public License
 * along with this library; see the file COPYING.LIB.  If not, write to
 * the Free Software Foundation, Inc., 51 Franklin Street, Fifth Floor,
 * Boston, MA 02110-1301, USA.
 */

#include "upnphttpserver.h"
#include "upnpservereventobject.h"
#include "upnpcontrolabstractservice.h"

class UpnpHttpServerPrivate
{
public:

    UpnpControlAbstractService *mService;
};

UpnpHttpServer::UpnpHttpServer(QObject *parent) : KDSoapServer(parent), d(new UpnpHttpServerPrivate)
{
    d->mService = nullptr;
}

UpnpHttpServer::~UpnpHttpServer()
{

}

QObject *UpnpHttpServer::createServerObject()
{
    auto newObject = new UpnpServerEventObject;
    newObject->setService(d->mService);
    return newObject;
}

void UpnpHttpServer::setService(UpnpControlAbstractService *service)
{
    d->mService = service;
}

#include "moc_upnphttpserver.cpp"
